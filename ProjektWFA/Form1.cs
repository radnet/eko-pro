﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace ProjektWFA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private class Dane
        {
            public String y { get; set; }
            public String[] x { get; set; }
        }

        private class Bledy
        {
            public String numer { get; set; }
            public String wartosc { get; set; }
        }

        private class szPara
        {
            public String b { get; set; }
        }

        Dane[] dane_wykresu;
        String[] wiersze;
        String[] kolumny;

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {


                String plik = openFileDialog.FileName;
                try
                {
                    String zawartosc = File.ReadAllText(plik);
                    CultureInfo ci = CultureInfo.CurrentCulture;
                    if (String.Equals(ci.Name, "pl-PL"))
                    {
                        zawartosc = zawartosc.Replace(".", ",");
                    }
                    String[] separator = new String[] { "\r\n" };
                    wiersze = zawartosc.Split(separator, StringSplitOptions.None);
                    kolumny = wiersze[0].Split(' ');
                    Dane[] dane = new Dane[wiersze.Length];
                    dane_wykresu = new Dane[wiersze.Length];

                    int liczba_xi = wiersze[0].Split(' ').Length - 1;

                    for (int i = 0; i < wiersze.Length; i++)
                    {
                        dane[i] = new Dane();
                        dane[i].x = new String[liczba_xi];
                    }
                   
                    for (int i = 0; i < wiersze.Length; i++)
                    {
                        dane[i].y = wiersze[i].Split(' ')[0];
                        
                        for (int j = 1; j<=liczba_xi; j++)
                        {
                            dane[i].x[j-1] = wiersze[i].Split(' ')[j];
                        }
                    }

                   //dane_wykresu = dane;
                    dataGrid.Rows.Clear();
                    dataGrid.Columns.Clear();
                   // dataGrid.DataSource = dane;
                    dataGrid.Columns.Add("y", "y");
                    for(int i =1; i<= liczba_xi;i++)
                    {
                        dataGrid.Columns.Add("x"+i, "x"+i);
                    }
                    dataGrid.Rows.Add(wiersze.Length-1);


                    for (int i = 0; i < wiersze.Length; i++)
                    {
                        //dataGrid.Rows.Add(new object[] { dane[i].y, dane[i].y });
                       
                        dataGrid.Rows[i].Cells[0].Value = dane[i].y;
                        for (int j = 0; j <liczba_xi; j++)
                        {
                            
                            dataGrid.Rows[i].Cells[j+1].Value = dane[i].x[j];
                        }
                       
                    }

                    //dataGrid.Columns.Add("aa2", "bb2");

                   
                    dataGrid.ReadOnly = true;
                   // dataGrid.AutoSize = true;
                    oblicz();
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }


        private void oblicz()
        {

            double[,] dane_do_macierzy = new double[wiersze.Length, kolumny.Length];
            for (int i = 0; i < wiersze.Length; i++)
            {
                for (int j = 0; j < kolumny.Length; j++)
                {
                    dane_do_macierzy[i, j] = Convert.ToDouble(wiersze[i].Split(' ')[j]);
                }
            }

            Matrix<double> dane = DenseMatrix.OfArray(dane_do_macierzy);

            Matrix<double> X = dane.RemoveColumn(0);
            Vector<double> y = dane.Column(0);

            double[] tab_jed = new double[wiersze.Length];
            for (int i = 0; i < wiersze.Length; i++)
            {
                tab_jed[i] = 1;
            }
            Vector<double> jedynki = DenseVector.OfArray(tab_jed);
            X = X.InsertColumn(0, jedynki);

            Matrix<double> ODWxtx = (X.Transpose() * X).Inverse();

            Vector<double> a = ODWxtx * X.Transpose() * y;

            double wari = (y * y - y * X * a) / (wiersze.Length - kolumny.Length); // S^2

            //odchylenie standardowe składnika resztowego
            double odchyl = Math.Round(Math.Sqrt(wari), 3);// S

            Matrix<double> d2a = wari * ODWxtx;

            Vector<double> przekatna = d2a.Diagonal();
            double[] bledy_srednie = new double[przekatna.Count];
            for (int i = 0; i < przekatna.Count; i++)
            {
                bledy_srednie[i] = Math.Sqrt(przekatna[i]);
            }

            double srednia_y = y.Average();
            
            double wspolczynnik_zbieznosci = (y * y - y * X * a) / ((y - srednia_y) * (y - srednia_y)); //fi^2

            double wspolczynnik_determinacji = 1 - wspolczynnik_zbieznosci; // R^2

            double wsp_zmien_los_V = (odchyl / srednia_y) * 100; // w procentach


            //WYKRES x1-linia x2- punkty
            chart1.Series["y=a[0]+a[1]*x1+a[2]*x2"].Points.Clear();
            chart1.Series["y(x1)"].Points.Clear();


            Vector<double> x1 = X.Column(1);
            Vector<double> x2 = X.Column(2);
            double min_x1 = x1.Min();
            double max_x1 = x1.Max();
            double min_x2 = x2[x1.MinimumIndex()];
            double max_x2 = x2[x1.MaximumIndex()];
            double y1, y2;
            y1 = a[0] + (a[1] * min_x1) + (a[2] * min_x2);
            y2 = a[0] + (a[1] * max_x1) + (a[2] * max_x2);
            chart1.Series["y=a[0]+a[1]*x1+a[2]*x2"].Points.AddXY(min_x1, y1);
            chart1.Series["y=a[0]+a[1]*x1+a[2]*x2"].Points.AddXY(max_x1, y2);

            for(int i=0;i<wiersze.Length;i++)
            {
                chart1.Series["y(x1)"].Points.AddXY(x1[i], y[i]);
            }

            label4.Text += Math.Round(odchyl,4)+"";
            label5.Text += Math.Round(wspolczynnik_determinacji,4) + "";
            label6.Text += Math.Round(wsp_zmien_los_V,4) + "%";

            Bledy[] bledy = new Bledy[bledy_srednie.Length];
            for(int i=0;i<bledy_srednie.Length;i++)
            {
                bledy[i] = new Bledy();
                bledy[i].numer = i+"";
                bledy[i].wartosc = Math.Round(bledy_srednie[i],4)+"";
            }
            dataGridView1.DataSource = bledy;

            szPara[] szacunki = new szPara[a.Count];
            for(int i=0; i<a.Count;i++)
            {
                szacunki[i] = new szPara();
                szacunki[i].b = Math.Round(a[i],4)+"";
            }
            dataGridView2.DataSource = szacunki;

            dataGridView1.ReadOnly = true;
            //dataGridView1.AutoSize = true;
            dataGridView2.ReadOnly = true;
            //dataGridView2.AutoSize = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
